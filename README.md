# Workshop Kubernetes (CCE) Demo App
GitHub/GitLab + Jenkins CI/CD Pipeline + k8s (CCE)

# PRINC CI/CD v1 Overview
![PRINC CI/CD v1](images/princ_v1.drawio.png)

# Prepair your computer
<!-- 1.Install aws cli
https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html -->

1.Install kubectl
https://kubernetes.io/docs/tasks/tools/

2.Copy demo-princ-kubeconfig.yaml to ~/.kube/config

<!-- 3.Insert aws credentials to ~/.aws/credentials
```
[princ-nonprod]
aws_access_key_id=XXX
aws_secret_access_key=XXX
``` -->

<!-- 4.Update kubeconfig (~/.kube/config) with aws cli
```
aws eks update-kubeconfig --name princ-nonprod \
--region ap-southeast-1 \
--profile princ-nonprod
``` -->

3.Test 
```
kubectl get node

# Output
NAME           STATUS   ROLES    AGE   VERSION
10.168.0.165   Ready    <none>   14h   v1.27.2-r0-27.0.6
10.168.0.194   Ready    <none>   14h   v1.27.2-r0-27.0.6
10.168.0.64    Ready    <none>   14h   v1.27.2-r0-27.0.6
```

4.Install Lens (Options)
https://k8slens.dev/

- Open Lens add princ-nonprod cluster

# Agenda
- Overview k8s 
- What's k8s?
  - Open source container orchestration tool
  - Developed by Google
  - Help you manage containerized applications in different deployment ENV
- Why k8s?
- Who use k8s?
https://kubernetes.io/case-studies/
- Workshop 1 (Deploy k8s App with kubectl)
- Workshop 2 (Deploy k8s App with Jenkins CI/CD Pipeline)
- Q & A


# Workshop 1 (Deploy k8s App with kubectl)
- kubectl get all, ns, deployment, pod, service
- kubectl create ns
- kubectl create deployment
- kubectl expose deployment
- kubectl scale deployment
- kubectl delete service
- kubectl delete deployment
- kubectl delete ns

- Lens k8s Dashboard & Monitoring

Options (ถ้าเวลาเหลือ)
- k8s secret, configmaps
- k8s pv, pvc
- Grafana + Prometheus k8s Dashboard & Monitoring



# Labs
```
# Show Cluster Info
kubectl cluster-info

# Show Namespace
kubectl get ns

# Create Namespace
kubectl create ns workshop

# Show Deployment
kubectl get deployments -n workshop

# Create Deployment
kubectl create deployment [yourname]-web --image=nginx -n workshop

# Show Pods
kubectl get pod -n workshop

# Port Forward
kubectl port-forward [yourname]-web-xxx 8080:80 -n workshop

# Show Logs
kubectl logs -f [yourname]-web-xxx -n workshop

# Expose Service
kubectl expose deployment [yourname]-web --port=80 --target-port=80 -n workshop

# Create Ingress
cat <<EOF | kubectl apply -f -
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: [yourname]-web
  namespace: workshop
  annotations:
    kubernetes.io/ingress.class: nginx
spec:
  rules:
    - host: [yourname]-workshop.princhealth.tech
      http:
        paths:
          - path: /
            pathType: Prefix
            backend:
              service:
                name: [yourname]-web
                port:
                  number: 80
EOF


# Check App
curl -H "Host: tono-workshop.princhealth.tech" 159.138.238.115

# Scale App
kubectl scale deployment/[yourname]-web --replicas=2 -n workshop

# Delete Ingress
kubectl delete ingress [yourname]-web -n workshop

# Delete Service
kubectl delete service [yourname-web] -n workshop

# Delete Deployment
kubectl delete deployment [yourname]-web -n workshop
```
 
# Workshop 2 (Deploy k8s App with Jenkins CI/CD Pipeline)
- Deploy k8s App with Jenkins CI/CD Pipeline
- Deploy k8s App in Dev ENV (build, tag, push, pull images)
- Deploy k8s App in QA ENV (from images tag)
- Auto Deploy from GitHub/GitLab Trigger to Jenkins

## [GitLab]
1. Create your repository https://gitlab.com/princ-dev/workshop/[name-app]
2. Fork from https://gitlab.com/ton-princ/k8s-workshop to your repository
3. Edit Jenkinsfile
- APP_GIT_URL = "https://gitlab.com/princ-dev/workshop/[name-app]"
- APP_BRANCH = "main"
- APP_NAME = "[name-app]"
- DEV_PROJECT = "dev"
- ECR_SERVER = "swr.ap-southeast-2.myhuaweicloud.com/princhealth"

## [Jenkins]
https://cicd.princhealth.tech/

4. Login with Google/Gmail
5. Create Job under folder workshop with name DEV-[name-app]

## [ECR]
6. Create docker repository princ/[name-app]

## [Lens]
7. Create secret [name-app] with credentials
- ELASTIC_APM_SERVICE_NAME = "[name-app]-workshop"
- ELASTIC_APM_SECRET_TOKEN = "XXX"
- ELASTIC_APM_SERVER_URL = "https://XXX.apm.us-central1.gcp.cloud.es.io:443" 

## [CloudFlare]
8. Create subdomain [name-app].princhealth.tech

# Jenkins
https://cicd.princhealth.tech/
- u: -
- p: -

# Grafana Dashboard
https://grafana.princhealth.tech/
- u: -
- p: -

# Datadog
https://app.datadoghq.com/
- u: -
- p: -

### Ref:
- Datadog Kubernetes Injecting Libraries Locally
 https://docs.datadoghq.com/tracing/trace_collection/library_injection_local/?tab=kubernetes
- Kubernetes Tutorial for Beginners [FULL COURSE in 4 Hours] https://www.youtube.com/watch?v=X48VuDVv0do
- DO280
https://www.redhat.com/en/services/training/do280-red-hat-openshift-administration-II-operating-production-kubernetes-cluster
- DO288
https://www.redhat.com/en/services/training/do288-red-hat-openshift-development-ii-containerizing-applications